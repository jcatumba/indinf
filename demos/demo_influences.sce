// ====================================================================
// Copyright (C) 2012 - Jorge Catumba Ruiz
// 
// This file must be used under the terms of the GPLv3.
// This source file is licensed as described in the file license.txt, which
// you should have received as part of this distribution.
// ====================================================================

function demo_influences()

    mprintf("How to use micmac, pagerank, pwp_method and heatkernel.\n")

    // Define a real square matrix, it must be an adjacency
    // matrix of some graph you are studing.
    A = [
    0 3 5 1
    3 0 0 0
    0 7 0 7
    3 0 8 1
    ];
    mprintf("We create a 4x4 real matrix.\n");
    disp(A);
    
    // Compute the micmac method
    [Amicmac,Admicmac,Afmicmac]=micmac(A,6);
    mprintf("We compute the micmac method for the A matrix.\n");
    disp(Amicmac);
    disp(Admicmac);
    disp(Afmicmac);
    
    // Compute the pagerank method with limit
    Adpagerank=pagerank(A,0.86,'limit');
    mprintf("We compute the pagerank method for the A matrix.\n");
    disp(Adpagerank);
    
    // Compute the pagerank method with matlab 
    Adpagerank=pagerank(A,0.86,'matlab');
    mprintf("We compute the pagerank method for the A matrix.\n");
    disp(Adpagerank);
    
    // Compute the pwp method
    [Apwp,Adpwp,Afpwp]=pwp_method(A,1);
    mprintf("We compute the pwp method for the A matrix.\n");
    disp(Apwp);
    disp(Adpwp);
    disp(Afpwp);
    
    // Compute the heatkernel method
    [Aheatkernel,Adheatkernel,Afheatkernel]=heatkernel(A,1);
    mprintf("We compute the heatkernel method for the A matrix.\n");
    disp(Aheatkernel);
    disp(Adheatkernel);
    disp(Afheatkernel);
    
    //
    // Load this script into the editor
    //
    filename = "demo_influences.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );

endfunction 
demo_influences();
clear demo_influences;
