// ====================================================================
// Copyright (C) 2012 - Jorge Catumba Ruiz
// 
// This file must be used under the terms of the GPLv3.
// This source file is licensed as described in the file license.txt, which
// you should have received as part of this distribution.
// ====================================================================

// Trying pwp_method(Matrix,Index)

A=[
0 2 1 0
3 0 1 4
0 0 0 0
2 4 1 0
];
expT=[
14.724763    16.510478    9.041555     13.037916
37.803633    40.800596    22.136579    33.020956
0            0            0            0        
36.067353    39.539914    21.12513     31.022159
];
Ah=pwp_method(A,1);
assert_checkalmostequal(Ah,expT,0,1D-5,"element");

// Trying [T,d,f]=pwp_method(Matrix,Index)

expf=[
0.2814073    0.3076285    0.1661312    0.2448330
];
expd=[
0.1693439    0.4248684    0    0.4057877
];
[T,d,f]=pwp_method(A,1);
assert_checkalmostequal(f,expf,0,1D-5,"element");
assert_checkalmostequal(d,expd,0,1D-5,"element");

// Trying pwp_method(PolyMatrix,Index,Time)

t=poly(0,'t');
B=[
0 1-t t^2
t 0 3-t
t^2 2 0
];
expT=[
0    1.3656846    1.4067501  
0    2.8135003    4.0970539  
0    2.7313693    2.8135003
];
Bh=pwp_method(B,1,0);
assert_checkalmostequal(Bh,expT,0,1D-5,"element");

// Trying [T,d,f]=pwp_method(PolyMatrix,Index,Time)
expf=[
0    0.4538100    0.5461900
];
expd=[
0.1820633    0.4538100    0.3641267
];
[T,d,f]=pwp_method(B,1,0);
assert_checkalmostequal(f,expf,0,1D-5,"element");
assert_checkalmostequal(d,expd,0,1D-5,"element");
