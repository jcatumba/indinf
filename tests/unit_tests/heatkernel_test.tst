// ====================================================================
// Copyright (C) 2012 - Jorge Catumba Ruiz
// 
// This file must be used under the terms of the GPLv3.
// This source file is licensed as described in the file license.txt, which
// you should have received as part of this distribution.
// ====================================================================

// Trying heatkernel(Matrix,Index)

A=[
0 2 1 0
3 0 1 4
0 0 0 0
2 4 1 0
];
expT=[
9.6757051 10.436613 5.7153528 8.2415349
23.896454 26.158775 13.992987 20.873225
0. 0. 0.3678794 0.
22.798915 24.993993 13.353629 19.977624 
];
Ah=heatkernel(A,1);
assert_checkalmostequal(Ah,expT,0,1D-5,"element");

// Trying [T,d,f]=heatkernel(Matrix,Index)

expf=[
0.2811768 0.3072055 0.1667468 0.2448709
];
expd=[
0.1699359 0.4235849 0.0018350 0.4046442
];
[T,d,f]=heatkernel(A,1);
assert_checkalmostequal(f,expf,0,1D-5,"element");
assert_checkalmostequal(d,expd,0,1D-5,"element");

// Trying heatkernel(PolyMatrix,Index,Time)

t=poly(0,'t');
B=[
0 1-t t^2
t 0 3-t
t^2 2 0
];
expT=[
0.3678794 0.8632773 0.8892357  
0. 2.1463508 2.589832   
0. 1.7265547 2.1463508
];
Bh=heatkernel(B,1,0);
assert_checkalmostequal(Bh,expT,0,1D-5,"element");

// Trying [T,d,f]=heatkernel(PolyMatrix,Index,Time)
expf=[
0.0342868 0.4414177 0.5242955
];
expd=[
0.1976230 0.4414177 0.3609593
];
[T,d,f]=heatkernel(B,1,0);
assert_checkalmostequal(f,expf,0,1D-5,"element");
assert_checkalmostequal(d,expd,0,1D-5,"element");
