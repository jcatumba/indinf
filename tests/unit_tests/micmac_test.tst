// ====================================================================
// Copyright (C) 2012 - Jorge Catumba Ruiz
// 
// This file must be used under the terms of the GPLv3.
// This source file is licensed as described in the file license.txt, which
// you should have received as part of this distribution.
// ====================================================================

// Trying micmac(Matrix,Index)

A=[
0 2 1 0
3 0 1 4
0 0 0 0
2 4 1 0
];
expT=[
448     968     340    128
1580    704     772    1936
0       0       0      0
1160    2000    800    608
];
Ah=micmac(A,5);
assert_checkalmostequal(Ah,expT,0,1D-5,"element");

// Trying [T,d,f]=micmac(Matrix,Index)

expf=[
0.2777778    0.3333333    0.1666667    0.2222222
];
expd=[
0.1666667    0.4444444    0    0.3888889
];
[T,d,f]=micmac(A,1);
assert_checkalmostequal(f,expf,0,1D-5,"element");
assert_checkalmostequal(d,expd,0,1D-5,"element");

// Trying micmac(PolyMatrix,Index,Time)

t=poly(0,'t');
B=[
0 1-t t^2
t 0 3-t
t^2 2 0
];
expT=[
0    36    0
0    0     108
0    72    0
];
Bh=micmac(B,5,0);
assert_checkalmostequal(Bh,expT,0,1D-5,"element");

// Trying [T,d,f]=micmac(PolyMatrix,Index,Time)
expf=[
0    0.5    0.5
];
expd=[
0.1666667    0.5    0.3333333
];
[T,d,f]=micmac(B,5,0);
assert_checkalmostequal(f,expf,0,1D-5,"element");
assert_checkalmostequal(d,expd,0,1D-5,"element");
