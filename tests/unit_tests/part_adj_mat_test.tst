// ====================================================================
// Copyright (C) 2012 - Jorge Catumba Ruiz
// 
// This file must be used under the terms of the GPLv3.
// This source file is licensed as described in the file license.txt, which
// you should have received as part of this distribution.
// ====================================================================

// Trying part_adj_mat(Matrix,Vlist)

A=[
0 2 1 0
3 0 1 4
0 0 0 0
2 4 1 0
];
Vlist=list([1,4]);
expPart=[
0    6    2
7    0    1
0    0    0
];
Apart=part_adj_mat(A,Vlist);
assert_checkalmostequal(Apart,expPart,0,1D-5,"element");
