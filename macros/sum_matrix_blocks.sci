function A = sum_matrix_blocks(M,vect)
// Returns a square matrix with the sum of the blocks of another square matrix.
//
// Calling Sequence
//  sum_matrix_blocks(M,vect)
//  A = sum_matrix_blocks(M,vect)
//
// Parameters
// M: Square matrix to calculate the block's sum.
// vect: Vector of the sizes of each squared block. Note that the sum of all entries of this vector must be equal to the size of the M matrix.
//
// Examples
// vect=[3 1]
// M = [1 1 0 0;1 0 3 0;2 0 1 0;5 0 0 1]
// sum_matrix_blocks(M,vect)
//
// Authors
//  Jorge Catumba ;

    j=size(M,'c')
    i=size(M,'r')
    r=size(vect,"r")
    c=size(vect,"c")
    ppsum=0
    if (r*c~=c & r*c~=r & typeof(v)~='constant') then
        error("Invalid input. Must be a constant vector.")
    else
        if (r>c) then
            vect=vect'
        end
        sumvect=vect*ones(1,size(vect,'c'))'
        if (j~=i | sumvect~=j) then
            error('Non square matrix or Incompatible vector')
        else
            columns=size(vect,'c')
            vectors=zeros(columns,j)
            for index=1:columns
                psum=sum(vect(1,1:index))
                for index2=1+ppsum:psum
                    vectors(index,index2)=1
                end
                ppsum=psum
            end
            
            A=zeros(columns,columns)
            for index=1:columns
                for index2=1:columns
                    A(index,index2)=vectors(index,:)*M*vectors(index2,:)'
                end
            end
        end
    end
    
endfunction
