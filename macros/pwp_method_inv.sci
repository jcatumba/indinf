function [T,d,f]=pwp_method_inv(D,lambda,time)
// Returns the vectors of direct influences and direct dependences and the matrix of direct influences according with the inverse of the PWP method.
//
// Calling Sequence
//  [T,d,f] = pwp_method_inv(D,lambda)
//  [T,d,f] = pwp_method_inv(D,lambda,time)
//
// Parameters
//  time: Value for the time parameter desired in case of polynomial matrix. Is mandatory in such case.
//  lambda: Value of the parameter for the PWP method, look the bibliography.
//  D: Matrix of direct influences.
//  f: Name of the variable to store the indirect influences vector.
//  d: Name of the variable to store the indirect dependences vector.
//  T: Name of the variable to store the indirect influences matrix.
//
// Description
//  This function gives the vector of direct influences and dependences and the matrix of direct influences of a given matrix of indirect influences according with the PWP method.
//  If the matrix of indirect influences has polynomial entries the function needs the time parameter. For more info look the bibliography.
//
// Examples
//  D = [1 0 1;0 0 0;0 0 6] // Define the matrix of direct influences.
//  [T,d,f] = pwp_method_inv(D,1)
//  D = sparse([1,2;2,3;3,1;4,1],[1,2,4,1],[4,4]) // Define a sparse matrix
//  [T,d,f] = pwp_method_inv(D,1)
//  t = poly(0,'t')  // Define the variable t.
//  D = [t 1-t;t^2 1] // Define a 2x2 matrix with polynomial entries.
//  [T,d,f] = pwp_method_inv(D,1,0) // Apply the pwp_method_inv method to matrix D with time 0.
//  [T,d,f] = pwp_method_inv(D,1,1) // Apply the pwp_method_inv method to matrix D with time 1.
//
// See also
//  micmac
//  pagerank
//  pwp_method
//  heatkernel
//
// Authors
//  Jorge Catumba ;
//
// Bibliography
//  R. Díaz, Indirect Influences, Advanced Studies in Contemporary Mathematics 23  (2013)  29-41.

  j=size(D,"r")

  if (typeof(D)=='polynomial') then
    T1=(1/lambda)*polylogm(((exp(lambda)-1)*D)+eye(j,j))
    C=coeff(T1)
    n=size(T1,"c")
		m=size(C,"c")
		if (time > m/n-1) then
			error('The time parameter is to high')
		else
    	    T=C(:,time*n+1:(time+1)*n)
		end
  else
    T=(1/lambda)*logm(((exp(lambda)-1)*D)+eye(j,j))
  end
  
  d=sum(T,'c')'
  f=sum(T,'r')

endfunction
