function [d] = pagerank(A,p,keyword)
// Returns the vectors of indirect dependences according with the PageRank method.
//
// Calling Sequence
//  d = pagerank(D,p,keyword) // Show the vector of of indirect dependences d and the bar plot of the result using the keyword algorithm.
//
// Parameters
//  keyword: A word that define the method to use. Valid words are: limit, matlab, power and sparsepower. This is mandatory.
//  p: Value of the parameter for the PageRank method, look the bibliography.
//  D: Transpose of the adjacency matrix of a graph.
//  d: Name of the variable to store the indirect dependences vector.
//
// Description
//  This function gives the vector of indirect dependences and the matrix of indirect influences of a given matrix of direct influences according with the PageRank method. You only need to declare the adjacency matrix of the graph, the function makes all the process. This method does'nt support polynomial matrices.
//
// Examples
//  D = [1 0 1;0 0 0;0 0 6] // Define the transpose of the adjacency matrix of a graph.
//  d = pagerank(D,0.85,'limit')
//  d = pagerank(D,0.85,'matlab')
//  d = pagerank(D,0.85,'power')
//  d = pagerank(D,0.85,'sparsepower')
//  d = pagerank(D,0.85,'inverse')
//  D = sparse([1,2;2,3;3,1],[1,2,4]) // Define a sparse matrix
//  d = pagerank(D,0.85,'limit')
//  d = pagerank(D,0.85,'matlab')
//  d = pagerank(D,0.85,'power')
//  d = pagerank(D,0.85,'sparsepower')
//
// See also
//  micmac
//  pwp_method
//  pwp_method_inv
//  heatkernel
//  heatkernel_inv
//
// Authors
//  Jorge Catumba ;
//
// Bibliography
//  S. Brin, R. Motwani, L. Page, T. Winograd, The PageRank Citation Ranking: Bringing Order to the Web, Techreport, Stanford Digital Library Technologies Project, 1998.
//  R. Díaz, Indirect Influences, Advanced Studies in Contemporary Mathematics 23  (2013)  29-41.
//  A. Langville, C. Meyer, Deeper Inside PageRank, Internet Math. 1 (2004) 335-380.
//  C. Moler, Google PageRank. http://www.mathworks.com/moler/exm/chapters/pagerank.pdf

  j=size(A,"c")

  if (typeof(A) == 'polynomial') then
   error("The PageRank can not be used on a polinomial matrix.") 
  else
      if (typeof(A) == 'constant') then
          A=sparse(A)
      end

      c=sum(A,1)
      k=find(c~=0)
      D=sparse([k;k]',full(c(k).^(-1)),[j,j])
      e=ones(j,1)
      I=speye(j,j)
      x=e/j
      oldx=zeros(j,1)
      z=((1-p)*(full(c~=0))+(full(c==0)))/j
      Ap=p*A*D+e*z

      select keyword
      case 'limit' 
          for var=1:j
              out(1,var)=sum(A(:,var))
          end

          for fil=1:j
              for col=1:j
                  if (A(fil,col)~=0) then
                      Dlim(fil,col)=1/out(1,col)*A(fil,col)
                  else
                      Dlim(fil,col)=0
                  end
              end
          end

          cero=zeros(j,j)
          verdad=and(Dlim==cero,"r")

          for ind=1:j
              if (verdad(1,ind)==%T) then
                  Dbar(1:j,ind)=1/j
              else
                  Dbar(1:j,ind)=Dlim(1:j,ind)
              end
          end
          
          En=1/j*ones(j,j)
          
          T=(p*Dbar+(1-p)*En)^(50)
          d=sum(T,'c')
          d=d/j

      case 'matlab'
          x=(I-p*A*D)\e
          if (sum(x) ~= 0) then
              d=x/sum(x)
          else
              d=x
          end

      case 'power'
          while norm(x-oldx) > .01
              oldx=x
              x=Ap*x
          end
          if (sum(x) ~= 0) then
              d=x/sum(x)
          else
              d=x
          end

      case 'sparsepower'
          G=p*A*D
          while norm(x-oldx) > .01
              oldx=x
              x=G*x + e*(z*x)
          end
          if (sum(x) ~= 0) then
              d=x/sum(x)
          else
              d=x
          end
      end
  end

endfunction
