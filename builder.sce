// Copyright (C) 2011 - Universidad Nacional de Colombia
// This file is released under the GPL licence

mode(-1);
lines(0);

TOOLBOX_NAME  = "indinf";
TOOLBOX_TITLE = "Indirect Influences for Graphs";
toolbox_dir   = get_absolute_file_path("builder.sce");

// Check Scilab's version
// =============================================================================

try
	v = getversion("scilab");
catch
	error(gettext("Scilab 5.2 or more is required."));
end

// Check development_tools module avaibility
// =============================================================================

if ~with_module('development_tools') then
  error(msprintf(gettext('%s module not installed.'),'development_tools'));
end

// Action
// =============================================================================

tbx_builder_macros(toolbox_dir);
tbx_builder_help(toolbox_dir);
if v(1)<6 & v(2)<0 then
    tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
    tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);
else
    tbx_build_loader(toolbox_dir);
    tbx_build_cleaner(toolbox_dir);
end

// Clean variables
// =============================================================================

clear toolbox_dir TOOLBOX_NAME TOOLBOX_TITLE;
